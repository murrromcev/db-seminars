# Семинар 4. Сложные запросы, подзапросы. Целостность.

## 1. Сложные запросы и подзапросы

### 1.1. Структура запроса

**Порядок написания запроса**

```postgresql
SELECT
  [ALL | DISTINCT [ON (expression [, ...] )] ]
  [* | expression [AS output_name] [, ...] ]
[FROM from_item [, ...] ]
[WHERE condition]
[GROUP BY grouping_element [, ...]]
[HAVING condition]
[ORDER BY expression [ASC | DESC | USING operator] [NULLS {FIRST | LAST}]
     [, ...]]
[LIMIT {count | ALL}]
[OFFSET start [ROW | ROWS]]
[FETCH {FIRST | NEXT} [count] {ROW | ROWS} ONLY]
```

### 1.2 Ключевое слово `WITH`
`WITH` предоставляет способ записывать дополнительные операторы для применения в больших запросах. 
Эти операторы, которые также называют общими табличными выражениями (Common Table Expressions, CTE), 
можно представить как определения временных таблиц, существующих только для одного запроса. 
Более подробно про СТЕ будет на следующих семинарах.
**Пример**:
```postgresql
WITH 
    regional_sales AS (
        SELECT 
            region, 
            SUM(amount) AS total_sales
        FROM 
            orders
        GROUP BY 
            region
    ), 
    top_regions AS (
        SELECT 
            region
        FROM 
            regional_sales
        WHERE 
            total_sales > (SELECT SUM(total_sales)/10 FROM regional_sales)
   )
SELECT 
    region,
    product,
    SUM(quantity) AS product_units,
    SUM(amount) AS product_sales
FROM 
    orders
WHERE 
    region IN (SELECT region FROM top_regions)
GROUP BY 
    region, 
    product;
```


### 1.3 Подзапросы

**Подзапрос** — запрос, содержащийся в другом SQL-запросе. Запрос, содержащий другой подзапрос, называется *содержащим выражением*.

* Подзапрос всегда заключен в круглые скобки и обычно выполняется до содержащего выражения.
* Подзапросы могут вкладываться друг в друга.
* В `SELECT’e` подзапросы можно использовать во всех разделах, кроме `GROUP BY`.

**Классификация подзапросов:**
1. По взаимодействию с содержащим выражением:
    * Связанные (т.е. ссылающиеся на столбцы основного запроса):
        * Для написания таких запросов полезно использование алиасов. (`SELECT … AS T`)
        * Для случаев, когда в основном запросе и в подзапросе используется одна и та же таблица, использование алиасов обязательно!
        * Выполняются для каждой строки содержащего выражения.
    * Несвязанные (т.е. полностью самодостаточные и не зависящие от основного запроса) — выполняются перед выполнением содержащего выражения.
2. По результату выполнения:
    * Скалярные (1 столбец и 1 строка)
    * Нескалярные

**Использование подзапросов:**
![](img/img_1.png)

Примеры использования подзапросов в DELETE, UPDATE и INSERT:

```postgresql
DELETE FROM Classes
WHERE NOT EXISTS (SELECT * FROM Marks WHERE Marks.class_id = Classes.id);
```


```postgresql
UPDATE Classes
   SET featured = 1
 WHERE (SELECT count(*) FROM marks WHERE class_id = id) > 10;
```

```postgresql
INSERT INTO BestStudents2024(`name`, `surname`)
SELECT name, surname FROM Students
WHERE (SELECT AVG(mark) FROM Marks WHERE Students.id = Marks.student_id) > (SELECT AVG(mark) FROM Marks);
```


### 1.4 Предикаты (для подзапросов вида 1 столбец и несколько строк)

* `EXISTS` — Значением условия `EXISTS` является `TRUE` в том и только в том случае, когда мощность таблицы-результата подзапроса больше нуля, иначе значением условия является `FALSE`:
```postgresql
SELECT SupplierName
  FROM Suppliers
WHERE EXISTS(
        SELECT ProductName
          FROM Products
         WHERE SupplierId = Suppliers.supplierId
           AND Price < 20);
```

* `IN` — Предикат `IN` для подзапросов работает так же, как и для обычных запросов (проверка наличия значения в списке):
```postgresql
SELECT emp_id 
     , fname
     , lname
     , title
FROM employee
WHERE emp_id IN(
        SELECT superior_emp_id
          FROM employee);
```

* `ALL` — `TRUE`, если результат подзапроса пуст или значение предиката равно TRUE для каждой строки подзапроса; если хотя бы что-то `FALSE`, то вернет `FALSE`, во всех остальных случаях вернет `UNKNOWN`:
```postgresql
SELECT EMP_NO
  FROM EMP
 WHERE DEPT_NO = 65
   AND EMP_SAL >= ALL(
        SELECT EMP1.EMP_SAL
        FROM EMP AS EMP1
        WHERE EMP.DEPT_NO = EMP1.DEPT_NO);
```

* `ANY` — `FALSE`, если результат подзапроса пуст или значение условия равно `FALSE` для каждой строки подзапроса; если хотя бы что-то `TRUE`, то вернет `TRUE`, в остальных случаях вернет `UNKNOWN`:
```postgresql
SELECT EMP_NO
  FROM EMP
WHERE DEPT_NO = 65
  AND EMP_SAL > ANY(
        SELECT EMP1.EMP_SAL
        FROM EMP AS EMP1
        WHERE EMP.DEPT_NO = EMP1.DEPT_NO);
```

* `CREATE TABLE AS` — создаёт таблицу и наполняет её данными, полученными в результате выполнения `SELECT`. Столбцы этой таблицы получают имена и типы данных в соответствии со столбцами результата `SELECT` (хотя имена столбцов можно переопределить, добавив явно список новых имен столбцов).

```sql
CREATE TABLE NEW_TABLE AS
    SELECT *
      FROM OLD_TABLE;
```

`CREATE TABLE AS` напоминает создание представления, но на самом деле есть значительная разница: эта команда создает новую таблицу и выполняет запрос только раз, чтобы наполнить таблицу начальными данными. Последующие изменения в исходных таблицах запроса в новой таблице отражаться не будут. С представлением, напротив, определяющая его команда `SELECT` выполняется при каждой выборке из него.

### 1.5 Полезные функции и операторы SQL

Иногда бывает полезно использовать в запросе специальные функции:
* `IN` - принадлежность определенному набору значений:
`X IN (a1, a2, ..., an)` <span>&#8803;</span> X = a<sub>1</sub> or X = a<sub>2</sub> or ... or X = a<sub>n</sub>
* `BETWEEN` - принадлежность определенному интервалу значений:
`X BETWEEN A AND B` <span>&#8803;</span> (X >= A and X <= B) or (X <= A and X >= B)
* `IF ... THEN ... [ELSIF ... THEN ... ELSE ...] END IF` - ветвления, **пример**:
```postgresql
SELECT
    IF number = 0 THEN
        'zero'
    ELSIF number > 0 THEN
        'positive'
    ELSIF number < 0 THEN
        'negative'
    ELSE
        'NULL'
    END IF AS number_class
FROM
    numbers
```
* `CASE [...] WHEN ... THEN ... ELSE ... END CASE` - еще один аналог ветвлений, **пример**:
```postgresql
SELECT
    CASE 
        WHEN number = 0 THEN
            'zero'
        WHEN number > 0 THEN
            'positive'
        WHEN number < 0 THEN
            'negative'
        ELSE
            'NULL'
    END CASE AS number_class
FROM
    numbers
```
* `DISTINCT ON` - исключает строки, совпадающие по всем указанным выражениям, **пример**:
```postgresql
-- вывести кол-во уникальных отделов
SELECT
    count(DISTINCT ON department_nm)
FROM
    salary;
```

#### 1.5.1 Поиск по шаблону

* `LIKE` - Выражение LIKE возвращает true, если строка соответствует заданному шаблону.

```sql
строка LIKE шаблон [ESCAPE спецсимвол]
строка NOT LIKE шаблон [ESCAPE спецсимвол]
```

Если шаблон не содержит знаков процента и подчёркиваний, тогда шаблон представляет в точности строку и LIKE работает как оператор сравнения. Подчёркивание (_) в шаблоне подменяет (вместо него подходит) любой символ; а знак процента (%) подменяет любую (в том числе и пустую) последовательность символов.

Например:

```sql
'abc' LIKE 'abc'    true
'abc' LIKE 'a%'     true
'abc' LIKE '_b_'    true
'abc' LIKE 'c'      false
```

Чтобы найти в строке буквальное вхождение знака процента или подчёркивания, перед соответствующим символом в шаблоне нужно добавить спецсимвол. По умолчанию в качестве спецсимвола выбрана обратная косая черта, но с помощью предложения ESCAPE можно выбрать и другой. Чтобы включить спецсимвол в шаблон поиска, продублируйте его.

* `SIMILAR TO` - возвращает true или false в зависимости от того, соответствует ли данная строка шаблону или нет. Он работает подобно оператору LIKE, только его шаблоны соответствуют определению регулярных выражений в стандарте SQL.

```sql
строка SIMILAR TO шаблон [ESCAPE спецсимвол]
строка NOT SIMILAR TO шаблон [ESCAPE спецсимвол]
```

Помимо средств описания шаблонов, позаимствованных от LIKE, SIMILAR TO поддерживает следующие метасимволы, унаследованные от регулярных выражений POSIX:

* Спецсимволы:
    
    * | означает выбор (одного из двух вариантов).

    * \* означает повторение предыдущего элемента 0 и более раз.

    * \+ означает повторение предыдущего элемента 1 и более раз.

    * ? означает вхождение предыдущего элемента 0 или 1 раз.

    * {m} означает повторяет предыдущего элемента ровно m раз.

    * {m,} означает повторение предыдущего элемента m или более раз.

    * {m,n} означает повторение предыдущего элемента не менее чем m и не более чем n раз.

    * Скобки () объединяют несколько элементов в одну логическую группу.

    * Квадратные скобки [...] обозначают класс символов так же, как и в регулярных выражениях POSIX.

*  Регулярные выражения POSIX - более мощное средство поиска по шаблонам, чем операторы LIKE и SIMILAR TO. Во многих командах Unix, таких как egrep, sed и awk используется язык шаблонов, похожий на описанный здесь. 



Оператор  | 	Описание | 	Пример 
--- | --- | ---
~ |	Проверяет соответствие регулярному выражению с учётом регистра	|'thomas' ~ '.*thomas.*'
~* | Проверяет соответствие регулярному выражению без учёта регистра | 'thomas' ~* '.*Thomas.*'
!~ | 	Проверяет несоответствие регулярному выражению с учётом регистра | 'thomas' !~ '.*Thomas.*'
!~* | Проверяет несоответствие регулярному выражению без учёта регистра | 'thomas' !~* '.*vadim.*'

[Подробнее про выражения POSIX в SQL](https://postgrespro.ru/docs/enterprise/11/functions-matching#FUNCTIONS-POSIX-REGEXP)

[Практика регулярных выражений](https://regex101.com/)

#### 1.5.2 Строковые функции

* `string || string` - конкатенация строк
* `LOWER(text)` - 	Переводит символы строки в нижний регистр
* `UPPER(text)` - 	Переводит символы строки в верхний регистр
* `SUBSTRING(string [from int] [for int])` - извлекает подстроку (например, `substring('Thomas' from 2 for 3) == hom`)
* `TRIM([leading | trailing | both] [characters] from string)` - Удаляет наибольшую подстроку, содержащую только символы characters (по умолчанию пробелы), с начала (leading), с конца (trailing) или с обеих сторон (both, (по умолчанию)) строки string
* `split_part(string text, delimiter text, field int)` - Разделяет строку string по символу delimiter и возвращает элемент по заданному номеру (считая с 1)

```sql
split_part('abc~@~def​~@~ghi', '~@~', 2)

--def
```

* `FORMAT(formatstr text [, formatarg "any" [, ...] ])` - выдаёт текст, отформатированный в соответствии со строкой формата, подобно функции sprintf в C.

[Подробнее про строковые функции](https://postgrespro.ru/docs/enterprise/11/functions-string)

#### 1.5.3 Операторы и функции даты/времени

* `CURRENT_DATE` - Текущая дата
* `NOW()` - Текущая дата и время (на момент начала транзакции)

* `EXTRACT(field FROM source)` - получает из значений даты/времени поля, такие как год или чаc.

Например:
```sql
SELECT EXTRACT(DAY FROM TIMESTAMP '2001-02-16 20:38:40');
Результат:16

SELECT EXTRACT(HOUR FROM TIMESTAMP '2001-02-16 20:38:40');
Результат:20

SELECT EXTRACT(MONTH FROM TIMESTAMP '2001-02-16 20:38:40');
Результат:2
```

* `pg_sleep(сек), pg_sleep_for(interval), pg_sleep_until(timestamp with time zone)` -  переводит процесс текущего сеанса в спящее состояние на указанное число секунд
```sql
SELECT pg_sleep(1.5);
SELECT pg_sleep_for('5 minutes');
SELECT pg_sleep_until('tomorrow 03:00');
```

[Подробнее про функции даты и времени в SQL](https://postgrespro.ru/docs/enterprise/11/functions-datetime)

#### 1.5.4 Математические операторы

* `abs(x)` - модуль числа
* `ceil(dp или numeric)` - ближайшее целое, большее или равное аргументу
* `power(a dp, b dp)` - a возводится в степень b
* `sqrt(dp или numeric)` - квадратный корень
* `random()` - случайное число в диапазоне 0.0 <= x < 1.0

Чаще всего операторы совпадают с аналогичными во всех остальных языках программирования.

[Все остальные математические операторы](https://postgrespro.ru/docs/enterprise/11/functions-math)

#### 1.5.5 Импорт и экспорт данных

* `COPY` - копировать данные между файлом и таблицей

```sql
COPY имя_таблицы [ ( имя_столбца [, ...] ) ]
    FROM { 'имя_файла' | PROGRAM 'команда' | STDIN }
    [ [ WITH ] ( параметр [, ...] ) ]

COPY { имя_таблицы [ ( имя_столбца [, ...] ) ] | ( запрос ) }
    TO { 'имя_файла' | PROGRAM 'команда' | STDOUT }
    [ [ WITH ] ( параметр [, ...] ) ]

Здесь допускается параметр:

    FORMAT имя_формата
    OIDS [ boolean ]
    FREEZE [ boolean ]
    DELIMITER 'символ_разделитель'
    NULL 'маркер_NULL'
    HEADER [ boolean ]
    QUOTE 'символ_кавычек'
    ESCAPE 'символ_экранирования'
    FORCE_QUOTE { ( имя_столбца [, ...] ) | * }
    FORCE_NOT_NULL ( имя_столбца [, ...] )
    FORCE_NULL ( имя_столбца [, ...] )
    ENCODING 'имя_кодировки'
```

COPY перемещает данные между таблицами PostgreSQL и обычными файлами в файловой системе. COPY TO копирует содержимое таблицы в файл, а COPY FROM — из файла в таблицу (добавляет данные к тем, что уже содержались в таблице). COPY TO может также скопировать результаты запроса SELECT.

Например, копирование можно выполнять таким образом:

```sql
 COPY tickets 
 FROM ‘C:\Users\User-N\Desktop\CSV\ticket_dataset_MOW.csv’ DELIMITER ‘,’ CSV HEADER;

 COPY (SELECT flight_id, flight_no, departure_airport, arrival_airport 
 FROM flights 
WHERE departure_airport = 'SVO') 
   TO ‘C:\Users\User-N\Desktop\CSV\flights_SVO.csv' CSV HEADER DELIMITER ','
```
* `pg_dump` -  программа для создания резервных копий базы данных Postgres Pro. Она создаёт целостные копии, даже если база параллельно используется. Программа pg_dump не препятствует доступу других пользователей к базе данных (ни для чтения, ни для записи). 

Программа pg_dump выгружает только одну базу данных. Чтобы сохранить глобальные объекты, относящиеся ко всем базам в кластере, например, роли и табличные пространства, воспользуйтесь программой pg_dumpall.

```pgsql
--Выгрузка базы данных mydb в файл SQL-скрипта: 
$ pg_dump mydb > db.sql

--Восстановление из ранее полученного скрипта в чистую базу newdb: 
$ psql -d newdb -f db.sql

-- Выгрузка всех баз данных: 
$ pg_dumpall > db.out
```

* В большинстве соверменных IDE также поддерживается импорт и экспорт данных во все современные форматы.

![](ing/img_2.jpg)


[Подробнее про COPY](https://postgrespro.ru/docs/enterprise/11/sql-copy)

[Подробнее про pg_dump](https://postgrespro.ru/docs/postgrespro/10/app-pgdump)

## 2. Целостность

### 2.1 Теоретическая справка (ограничения)

1. `NOT NULL` – значение всегда известно, недопустимо значение `NULL`.
2. `UNIQUE` – значения в столбце должны быть уникальны.
3. `PRIMARY KEY` – первичный ключ таблицы. В некоторых СУБД требуется дополнительно ограничивать `NOT NULL` (чаще выставлено по дефолту; также заведомо является `UNIQUE`)
4. `FOREIGN KEY` – внешний ключ, необходима ссылка на другую таблицу.
5. `CHECK` – проверка на соответствие определенному критерию.
6. `DEFAULT` – значение по умолчанию; используется, если пользователь не задал значения.

### 2.2 Синтаксис добавления ограничений

1. `NOT NULL`
```postgresql
CREATE TABLE PERSON (
    ID         INTEGER      NOT NULL,
    LAST_NAME  VARCHAR(255) NOT NULL,
    FIRST_NAME VARCHAR(255) NOT NULL,
    AGE        INTEGER
);
```

2. `UNIQUE`
```postgresql
CREATE TABLE PERSON (
    ID         INTEGER      NOT NULL UNIQUE,
    LAST_NAME  VARCHAR(255) NOT NULL,
    FIRST_NAME VARCHAR(255) NOT NULL,
    AGE        INTEGER
);

ALTER TABLE PERSON ADD UNIQUE (ID);

ALTER TABLE PERSON
ADD CONSTRAINT UC_Person UNIQUE (ID, LAST_NAME);

ALTER TABLE PERSON
DROP CONSTRAINT UC_Person;
```

3. `PRIMARY KEY`
```postgresql
CREATE TABLE PERSON (
    ID         INTEGER      PRIMARY KEY,
    LAST_NAME  VARCHAR(255) NOT NULL,
    FIRST_NAME VARCHAR(255) NOT NULL,
    AGE        INTEGER
);

ALTER TABLE PERSON ADD PRIMARY KEY (ID);

------------------------------------------

CREATE TABLE PERSON (
    ID         INTEGER,
    LAST_NAME  VARCHAR(255),
    FIRST_NAME VARCHAR(255) NOT NULL,
    AGE        INTEGER,
    CONSTRAINT PK_Person PRIMARY KEY (ID, LAST_NAME)
);

ALTER TABLE PERSON
ADD CONSTRAINT PK_Person PRIMARY KEY (ID, LAST_NAME);

ALTER TABLE PERSON
DROP CONSTRAINT PK_Person;
```

4. `FOREIGN KEY`
```postgresql
CREATE TABLE ORDER (
    ORDER_ID     INTEGER,
    ORDER_NUMBER INTEGER NOT NULL,
    PERSON_ID    INTEGER,
    
    PRIMARY KEY (ORDER_ID),
    CONSTRAINT FK_PersonOrder FOREIGN KEY (PERSON_ID) REFERENCES PERSON(PERSON_ID)
);

ALTER TABLE ORDER ADD CONSTRAINT FK_PersonOrder
FOREIGN KEY (PERSON_ID) REFERENCES PERSON(PERSON_ID);

ALTER TABLE ORDER DROP CONSTRAINT FK_PersonOrder;

------------------------------------------

CREATE TABLE ORDER (
    ORDER_ID     INTEGER PRIMARY KEY,
    ORDER_NUMBER INTEGER NOT NULL,
    PERSON_ID    INTEGER REFERENCES PERSON(PERSON_ID)
);

ALTER TABLE ORDER
ADD FOREIGN KEY (PERSON_ID) REFERENCES PERSON(PERSON_ID);
```

При определении FK у нас должно совпадать количество атрибутов в обеих таблицах, которые мы "мэтчим". Также в таблице, для которой атрибуты являются PK, должны быть заданы соответствующие ограничения – иначе будет ошибка `there is no unique constraint matching given keys for referenced table`.

<span>&ensp;&nbsp;</span>4<span>&frac34;</span>. Поддержание ссылочной целостности

* `CASCADE` – при удалении / изменении строки главной таблицы соответствующая запись дочерней таблицы также будет удалена / изменена.

* `RESTRICT` – cтрока не может быть удалена / изменена, если на нее имеется ссылка;  Значение не может быть удалено / изменено, если на него есть ссылка.

* `NO ACTION`

    * Имеет сходства с `RESTRICT`, но проверка происходит в конце транзакции;
    * Для разницы с RESTRICT нужно явно прописывать в транзакции выражение ([`SET CONSTRAINTS`](https://www.postgresql.org/docs/current/sql-set-constraints.html))

    ```postgresql
    SET CONSTRAINTS { ALL | name [, ...] } { DEFERRED | IMMEDIATE }
    ```

* `SET NULL` – при удалении записи главной таблицы, соответствующее значение дочерней таблицы становится `NULL`.

* `SET DEFAULT` – аналогично `SET NULL`, но вместо значения `NULL` устанавливается некоторое значение по умолчанию.

```postgresql
CREATE TABLE ORDER (
    ORDER_ID     INTEGER,
    ORDER_NUMBER INTEGER NOT NULL,
    PERSON_ID    INTEGER,
    
    PRIMARY KEY (ORDER_ID),
    CONSTRAINT FK_PersonOrder FOREIGN KEY (PERSON_ID)
        REFERENCES PERSON(PERSON_ID)
            ON DELETE RESTRICT
            ON UPDATE RESTRICT
);
```

5. `CHECK`
```postgresql
CREATE TABLE PERSON (
    ID         INTEGER      NOT NULL,
    LAST_NAME  VARCHAR(255) NOT NULL,
    FIRST_NAME VARCHAR(255) NOT NULL,
    AGE        INTEGER      CHECK (AGE >= 18)
);

ALTER TABLE PERSON ADD CHECK (AGE >= 18);

------------------------------------------

CREATE TABLE PERSON (
    ID          INTEGER      NOT NULL,
    LAST_NAME   VARCHAR(255) NOT NULL,
    FIRST_NAME  VARCHAR(255) NOT NULL,
    AGE         INTEGER,
    CITY        VARCHAR(255),
    CONSTRAINT CHK_Person CHECK (AGE >= 18 AND CITY = 'Moscow')
);

ALTER TABLE PERSON ADD CONSTRAINT CHK_Person
CHECK (AGE >= 18 AND CITY = 'Moscow');

ALTER TABLE PERSON DROP CONSTRAINT CHK_Person;
```

6. `DEFAULT`
```postgresql
CREATE TABLE ORDER (
    ORDER_ID     INTEGER PRIMARY KEY,
    ORDER_NUMBER INTEGER NOT NULL,
    ORDER_DATE   DATE    DEFAULT now()::date
);

ALTER TABLE ORDER;
ALTER COLUMN ORDER_DATE DROP DEFAULT;
```

Есть возможность посмотреть все **имеющиеся в базе ограничения**:

```postgresql
-- Список колонок, попадающих под ограничение:
SELECT * FROM information_schema.constraint_column_usage;

-- Все имеющиеся в базе ограничения:
SELECT * FROM information_schema.table_constraints;

-- Уникальные и ключевые (PK, FK) поля таблиц
SELECT * FROM information_schema.key_column_usage;

-- Информация по ограничениям с типом `CHECK`
SELECT * FROM information_schema.check_constraints;

-- Информация по ограничениям с типом `DEFAULT` и `NOT NULL`
SELECT * FROM information_schema.columns;
```



## 3. Практическое задание (сложные запросы + подзапросы)

1. С помощью скалярного подзапроса найти имена преподавателей, которые получили разово минимальную выплату за все время.
2. С помощью скалярного подзапроса найти имена преподавателей, у которых выплата по тому или иному типу была максимальной. Вывести имя преподавателя и тип выплаты, используя case или decode (1 – выплата за семинарские занятия, 2 – выплата за лекционные занятия, 3 – премиальная часть);
3. С помощью подзапроса и предиката `IN` вывести имена преподавателей, тип и сумму выплат по каждому типу за все время работы, при
   условии, что у преподавателя была выплата за лекционные занятия.
4. С помощью `EXISTS` вывести имена преподавателей, тип и сумму выплат по каждому типу за все время работы, при условии, что у
   преподавателя была выплата за лекционные занятия;
5. С помощью аналитической функции `COUNT` найти количество выплат по каждому преподавателю за каждое число; выведите также размер каждой выплаты.
6. С помощью аналитической функции `SUM` найти общий размер выплат по каждому преподавателю за каждое число; выведите также размер каждой выплаты.
7. С помощью аналитической функции `SUM` и сортировки найти суммарные выплаты по каждому преподавателю за каждое число (по нарастанию).

## 3.1 Практическое задание (Полезные функции SQL)

1. Создайте схему `seminar_4_1`.
2. В этой схеме создайте таблицу `users` с полями:
   - `id` (целое число, primary key, serial),
   - `full_name` (строка, не может быть пустым),
   - `email` (строка, не может быть пустым, уникальное значение),
   - `created_at` (тип `timestamp`, не может быть пустым, по умолчанию `now()`).
3. Добавьте несколько пользователей с именами и email-адресами (по вашему выбору). Пример:

    ```sql
    insert into seminar_4_1.users (full_name, email) values
        ('Алексей Иванов', 'alexey@gmail.com'),
        ('Мария Петрова', 'maria.petrova@example.com'),
        ('Дмитрий Смирнов', 'dm1tr1y.sm1rnov@example.com'),
        ('Елена Смирнова', 'elena.smirnova@yahoo.com'),
        ('Михаил Сидоров', 'c00l_m0nkey_2005@mail.ru'),
        ('Ольга Иванова', 'olga.ivan0va@yandex.ru');
    ```

4. Напишите запрос, который найдет всех пользователей, чьи имена начинаются на "А".

5. Напишите запрос, который вернет пользователей, чьи email-адреса содержат домен "example.com".

6. Напишите запрос, который извлекает email без домена из поля `email` для всех пользователей из таблицы `users`.

7. Напишите запрос, который вернет пользователей, чьи email-адреса содержат хотя бы одну цифру.

8. Напишите запрос, который вернет пользователей, чьи имена начинаются с "М" и заканчиваются на "ова".

9. Напишите запрос, который вернет пользователей, чьи email-адреса имеют домен, начинающийся с буквы "e".

10. Создайте таблицу `orders` в схеме `seminar_4_1` с полями:
   - `order_id` (целое число, primary key, serial),
   - `user_id` (целое число, внешний ключ на таблицу `users`),
   - `order_date` (тип `timestamp`),
   - `total_amount` (тип `numeric`).
   
11. Вставьте несколько заказов с различными датами:

    ```sql
    insert into seminar_4_1.orders (user_id, order_date, total_amount) values
        (1, '2024-12-15 10:15:00', 300.50),
        (2, '2025-01-02 14:20:00', 150.75),
        (3, '2025-01-03 09:30:00', 450.00),
        (4, '2025-01-04 05:23:00', 310.99),
        (1, '2025-01-05 12:22:00', 130.49),
        (2, '2025-02-12 00:00:00', 850.20);
    ```

12. Напишите запрос, который возвращает все заказы, сделанные в январе 2025 года.

13. Напишите запрос, который находит заказы, сделанные за последние 7 дней от текущей даты.

14. Напишите запрос, который округляет цену каждого товара из таблицы `products` в большую сторону до ближайшего целого.

15. Напишите запрос, который находит среднюю сумму заказов, сделанных в январе 2025 года, с округлением до двух знаков после запятой.

16. Напишите запрос, который вычисляет сумму всех заказов и возводит результат в квадрат.

17. Напишите запрос, который находит все заказы, сумма которых больше в два раза средней суммы всех заказов.

## 4. Практическое задание (Целостность)

1. Создать схему `seminar_4_2`;
2. Создать в этой схеме таблицу `cast` с ненулевыми полями `name`  и  `surname`, а также `birth_date` типа `date`;
3. Запустить следующие операции вставки:
    
    ```sql
    insert into seminar_4_2.cast values
        ('Милли', 'Олкок', '2000-04-11'),
        ('Мэтт', 'Смит', '1982-10-28'),
        ('Эмма', 'Д''Арси', '1992-06-27'),
        ('Оливия', 'Кук', '1993-12-27'),
        ('Эмили', 'Кэри', '2003-04-30');
    insert into seminar_4_2.cast values
        ('Юэн', 'Митчелл'),
        ('Пэдди', 'Консидайн'),
        ('Ив', 'Бест');
    ```
    
4. Добавить `primary key` -колонку `id` типа `serial` ([документация](https://postgrespro.ru/docs/postgresql/9.6/datatype-numeric)). Как называется такой вид ключа?
5. Добавить ограничение на колонку  `birth_date` – значение по умолчанию равно `date ‘1900-01-01’`.
Выведите все строки таблицы, чтобы проверить значения колонок `date` и `id`. (В случае использования IDE также можно в схеме базы посмотреть на то, как выглядит схема таблицы)
6. Удалите все строки с пустой датой рождения, а после запустите вторую вставку. Снова выведите все строки таблицы и обратите внимание на колонки `date` и `id`.
7. Создайте в схеме `seminar_4_2` таблицу `characters` с ненулевым полем `name`,  внешним ключом  `actor_id`, ссылающимся на поле `id` из таблицы `seminar_4_2.cast`,  текстовым полем `comment`, полем `sex`, которое может принимать значения `'male'` или `'female'`.
8. Запустите следующие операции вставки в таблицу:
    
    ```sql
    insert into seminar_4_2.characters (name, actor_id, comment, sex) values
        ('Рейнира Таргариен', 1, 'Юная принцесса', 'female'),
        ('Деймон Таргариен', 2, '', 'male'),
        ('Рейнира Таргариен', 3, 'Принцесса / королева', 'female'),
        ('Алисента Хайтауэр', 4, 'Королева', 'female'),
        ('Алисента Хайтауэр', 5, 'Юная леди / королева', 'female'),
        ('Эймонд Таргариен', 9, 'Принц', 'male');
    ```
    
9. Попробуйте добавить в таблицу:
    1. строку с `actor_id`, который не встречается в таблице `seminar_4_2.cast`;
    2. строку с пустым полем `sex`: `('Эймонд Таргариен', 9)`;
    3. строку с пустым полем `actor_id`: `('Эймонд Таргариен')`;
10. Какое поле или набор полей подойдут на роль первичного ключа в этой таблице? Добавьте соответствующее ограничение, предварительно удалив строки, которые мешают это сделать. (В случае использования IDE также можно в схеме базы посмотреть на то, как теперь выглядит схема таблицы)
11. Удалите из таблицы `seminar_4_2.cast` актеров, игравших юных героев (содержащих в комментарии слово ‘юный’ или ‘юная’), так, чтобы все их герои также исчезли из таблицы `seminar_4_2.characters`. Возможно придется удалить созданное ограничение на внешний ключ и задать новое.


