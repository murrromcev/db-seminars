
--1, 2. Создание схемы и таблицы:
create schema seminar_4_1;

    create table seminar_4_1.users (
        id serial primary key,
        full_name varchar(255) not null,
        email varchar(255) not null unique,
        created_at timestamp not null default now()
    );

--3. Вставка данных:

    insert into seminar_4_1.users (full_name, email) values
        ('Алексей Иванов', 'alexey@gmail.com'),
        ('Мария Петрова', 'maria.petrova@example.com'),
        ('Дмитрий Смирнов', 'dm1tr1y.sm1rnov@example.com'),
        ('Елена Смирнова', 'elena.smirnova@yahoo.com'),
        ('Михаил Сидоров', 'c00l_m0nkey_2005@mail.ru'),
        ('Ольга Иванова', 'olga.ivan0va@yandex.ru');


--4. Поиск пользователей, чьи имена начинаются на "А":

    select * 
      from seminar_4_1.users
     where full_name like 'А%';


--5. Поиск пользователей, чьи email-адреса содержат домен "example.com":
 
    select * from seminar_4_1.users
    where email like '%@example.com';

--6. Извлечение email без домены

   select split_part(email, '@', 1) 
     from seminar_4_1.users;

--7. email-адреса содержат хотя бы одну цифру.

    select * 
      from seminar_4_1.users
     where email ~ '\d';

--8. Имена начинаются с "М" и заканчиваются на "ова".

    select * 
      from seminar_4_1.users
     where full_name like 'М%ова';

--9. email-адреса имеют домен, начинающийся с буквы "e".

    select * 
      from seminar_4_1.users
     where email ~ '@e';

--10. Создание таблицы `orders`:

    create table seminar_4_1.orders (
        order_id serial primary key,
        user_id int references seminar_4_1.users(id),
        order_date timestamp,
        total_amount numeric
    );

--11. Вставка данных в таблицу `orders`:
    insert into seminar_4_1.orders (user_id, order_date, total_amount) values
        (1, '2024-12-15 10:15:00', 300.50),
        (2, '2025-01-02 14:20:00', 150.75),
        (3, '2025-01-03 09:30:00', 450.00),
        (4, '2025-01-04 05:23:00', 310.99),
        (1, '2025-01-05 12:22:00', 130.49),
        (2, '2025-02-12 00:00:00', 850.20);

--12. Запрос на поиск заказов, сделанных в январе 2025 года:
    select * 
      from seminar_4_1.orders
     where order_date between '2025-01-01' and '2025-01-31';

    select * 
      from seminar_4_1.orders
     where extract(year from order_date) = 2025
       and extract(month from order_date) = 1;

--13. Запрос на поиск заказов, сделанных за последние 7 дней:
    select * 
      from seminar_4_1.orders
     where order_date >= current_date - interval '7 days';

--14. Запрос на округление цены товара в большую сторону:

    select product_name, ceil(price) as rounded_price
      from seminar_4_1.orders;

--15. Средняя сумма заказов за январь

    select round(avg(total_amount), 2) as average_order_amount
      from seminar_4_1.orders
     where extract(year from order_date) = 2025
       and extract(month from order_date) = 1;

--16. Сумма заказов в квадрате

    select power(sum(total_amount), 2) as squared_total_amount
      from seminar_4_1.orders;

--17. Больше среденй суммы заказа в 2 раза 

    select * 
      from seminar_4_1.orders
     where total_amount > (select avg(total_amount) * 2 from seminar_4_1.orders);